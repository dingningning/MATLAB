function s=diag_sum(A)
[m,n]=size(A)
a=min(m,n);
i=(a+1)/2;
A=A([1:a],[1:a]);
B=flip(A);
if mod(a,2)==0
    s=sum(diag(A))+sum(diag(B));
else
    s=sum(diag(A)+sum(diag(B))-A(i,i);
end

